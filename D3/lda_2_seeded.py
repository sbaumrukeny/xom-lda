# %%
import os
from datetime import datetime
from contextlib import contextmanager
from wordcloud import WordCloud

@contextmanager
def cwd(path):
    oldpwd=os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(oldpwd)

THEME = "Productive Inclusive Workplace - Feel supported"
SOURCE_FILE = "clean.csv"

RUN_NAME = f"{THEME}_{datetime.now().strftime('%Y%m%d-%H%M%S')}"

WORKING_DIR = os.path.join(THEME, RUN_NAME)

def fp(path):
    return os.path.join(WORKING_DIR, path)

with open(SOURCE_FILE, 'rb') as source_file:
    if not os.path.isdir(THEME):
        os.mkdir(THEME)
    if not os.path.isdir(WORKING_DIR):
        os.mkdir(WORKING_DIR)
    with open(fp('data.csv'),'wb') as result_file:
        result_file.write(source_file.read())

print(RUN_NAME)

# %%
# read data

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv(fp('data.csv'))

df.groupby('verbatim_type')['topics'].value_counts()

# %%
# filter on topics
filter_topics_includes = ["Productive Inclusive Workplace - Feel supported", "Productive Inclusive WI - Feel"]

print("Original Length:", len(df))
len(df.dropna(subset=['topics']))
df = df.dropna(subset=['topics'])
df = df[sum([df['topics'].str.contains(topic) for topic in filter_topics_includes])>=1]
vb = df['verbatim']
print("New length after filtering:", len(df))
print('\n',df['verbatim_type'].value_counts())

# %%
# imports

import gensim
from gensim.models.ldamulticore import LdaMulticore
from gensim import corpora, models
import pyLDAvis.gensim_models

import nltk
from nltk.corpus import stopwords
import string
from nltk.stem.wordnet import WordNetLemmatizer

import warnings
warnings.simplefilter('ignore')
from itertools import chain

# %%
# clean

from gensim.utils import simple_preprocess

nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')
nltk.download('omw-1.4')
stop = set(stopwords.words('english'))
exclude = set(string.punctuation)
lemma = WordNetLemmatizer()

def simplify(penn_tag):
    pre = penn_tag[0]
    if (pre == 'J'):
        return 'a'
    elif (pre == 'R'):
        return 'r'
    elif (pre == 'V'):
        return 'v'
    else:
        return 'n'

def clean(text):
    stop_free = ' '.join([word for word in text.lower().split() if word not in stop])
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    toks = simple_preprocess(punc_free, deacc=True)
    normalized = ' '.join([lemma.lemmatize(tok, simplify(pos)) for tok, pos in nltk.pos_tag(toks) if tok not in stop])
    return normalized.split()

df['clean'] = df.verbatim.apply(lambda x: clean(x))

# %%
dictionary = corpora.Dictionary(df.clean)
print('Unique Words:', dictionary.num_nnz)

# %%
doc_term_matrix = [dictionary.doc2bow(doc) for doc in df.clean]
print('Number of Key Terms:', len(doc_term_matrix))

# %%
generate_index = 0

# %%
# generate and dump model
import pickle

def create_eta(priors, etadict, ntopics):
    eta = np.full(shape=(ntopics, len(etadict)), fill_value=1) # create a (ntopics, nterms) matrix and fill with 1
    for word, topic in priors.items(): # for each word in the list of priors
        keyindex = [index for index,term in etadict.items() if term==word] # look up the word in the dictionary
        if (len(keyindex)>0): # if it's in the dictionary
            eta[topic,keyindex[0]] = 1e7  # put a large number in there
    eta = np.divide(eta, eta.sum(axis=0)) # normalize so that the probabilities sum to 1 over all topics
    return eta

apriori = {
    'environment': 1, 'atmosphere': 1, 'respecting': 2, 'health': 3, 'balance': 3, 'inclusion': 4, 'diversity': 4, 'hr': 5, 'assessment': 5, 'worklife': 3, 'home': 3, 'family': 3, 'empathy': 2, 'acceptance': 4, 'management': 0, 'leader': 0, 'leadership': 0, 'life': 3, 'travel': 3, 'productive': 3, 'manager': 0, 'time': 3
}

eta = create_eta(apriori, dictionary, 6)

NUMBER_OF_TIMES_TO_RUN = 1

for _ in range(NUMBER_OF_TIMES_TO_RUN):

    RUN_NAME = f"{THEME}_{datetime.now().strftime('%Y%m%d-%H%M%S')}"

    generate_index += 1

    MCONFIG = {
        'num_topics': 6,
        'passes': 500,
        'min_p': 0,
        'alpha': 'symmetric',
        'eta': eta,
        'random_state': generate_index
    }

    OUTPUT_BASE_NAME = f"{RUN_NAME} ({MCONFIG['random_state']}-{MCONFIG['num_topics']}-{MCONFIG['passes']})"

    OUTPUT_MODEL_FILE_PATH = fp(f"{OUTPUT_BASE_NAME}_model.model")

    print('Outputting to:', OUTPUT_MODEL_FILE_PATH)

    lda = gensim.models.ldamodel.LdaModel

    ldamodel = lda(doc_term_matrix, num_topics=MCONFIG['num_topics'], id2word=dictionary, passes=MCONFIG['passes'], minimum_probability=0, random_state=generate_index, alpha=MCONFIG['alpha'], eta=MCONFIG['eta'], eval_every=-1, update_every=1)
    with open(OUTPUT_MODEL_FILE_PATH, 'wb') as file:
        pickle.dump(ldamodel, file)

    # generate and dump visualization
    visualization_output_path = fp(f"{OUTPUT_BASE_NAME}_visualization.html")
    lda_display = pyLDAvis.gensim_models.prepare(ldamodel, doc_term_matrix, dictionary, sort_topics=False, mds='mmds')
    pyLDAvis.save_html(lda_display, visualization_output_path)

    # %%
    # load and view model
    import pickle

    INPUT_MODEL_FILE_PATH = OUTPUT_MODEL_FILE_PATH
    # INPUT_MODEL_FILE_PATH = 'C:\\Users\\SB579KE\\Dev\\Exxon\\D3\\Productive Inclusive Workplace - Feel supported\\Productive Inclusive Workplace - Feel supported_20220531-170447\\ldamodel_Productive Inclusive Workplace - Feel supported_20220531-170447 (3).model'

    print('Loading from:', INPUT_MODEL_FILE_PATH)

    with open(INPUT_MODEL_FILE_PATH, 'rb') as file:
        ldamodel = pickle.load(file)

    lda_display = pyLDAvis.gensim_models.prepare(ldamodel, doc_term_matrix, dictionary, sort_topics=True, mds='mmds')
    pyLDAvis.display(lda_display)

    # %%
    # add cluster fit probability columns to dataframe

    lda_corpus = ldamodel[doc_term_matrix]
    scores = list(chain(*[[score for topic_id, score in topic] for topic in [doc for doc in lda_corpus]]))
    threshold = sum(scores) / len(scores)
    print(threshold)
    prefix = 'cluster_fit_'
    c_scores = [doc for doc in lda_corpus]

    cluster_fit_cols = []

    for i in range(len(lda_corpus[0])):
        cluster_id = i + 1
        new_cluster_fit_col = f"{prefix}{str(cluster_id)}"
        cluster_fit_cols.append(new_cluster_fit_col)
        df[new_cluster_fit_col] = [c_scores[j][i][1] for j in range(len(lda_corpus))]


    df.to_csv(fp(RUN_NAME + '_results.csv'))

    # %%
    # assign best fit cluster

    result_best_fit_clusters = []

    for index, row in df.iterrows():
        cluster_fit_probabilities = [row[cluster_col_name] for cluster_col_name in cluster_fit_cols]
        best_fit_cluster_idx = max(range(len(cluster_fit_probabilities)), key=cluster_fit_probabilities.__getitem__)
        best_fit_cluster = 'cluster_fit_' + str(best_fit_cluster_idx+1)
        # best_fit_cluster = [cluster_col_name for cluster_col_name in cluster_fit_cols if row[cluster_col_name]>threshold][0]
        result_best_fit_clusters.append(best_fit_cluster)

    best_fit_cluster_name = f'{THEME}_best_fit_cluster'

    df[best_fit_cluster_name] = result_best_fit_clusters

    # %%
    # create wordclouds and excel

    # create wordclouds and excel

    from wordcloud import WordCloud, random_color_func
    import random

    def exxon_color_func(word, font_size, position, orientation, **kwargs):
        return "#fe000c"

    cluster_fit_names = df[f'{THEME}_best_fit_cluster'].unique().tolist()

    excel_output_path = fp(f"{RUN_NAME}_by_subtopic.xlsx")

    with pd.ExcelWriter(excel_output_path) as xlsx_writer:
        grouped_wordcloud_fig = plt.figure()
        colors = ["#0C479D", "#fe000c", "#6cc04a", "#ffd801", "#e612ad", "#12dee6", "#e66712", "#12e652"]
        random.shuffle(colors)
        i = 0
        for cluster_fit_name in sorted(cluster_fit_names):
            i += 1
            print(cluster_fit_name)
            # clean up the clean text for word cloud
            vb_current_cluster = df[df[best_fit_cluster_name]==cluster_fit_name].sort_values(cluster_fit_name, ascending=False)
            vb_current_cluster['clean_joined_text'] = [' '.join(clean_text_list) for clean_text_list in vb_current_cluster.clean.tolist()]

            # write to excel sheet
            sheet_name = f"{cluster_fit_name}"
            vb_current_cluster.to_excel(xlsx_writer, sheet_name=sheet_name, index=False)

            # generate word cloud
            word_cloud = WordCloud(collocations=False, background_color='white').generate(' '.join(vb_current_cluster.clean_joined_text.tolist()))
            n_clusters = len(cluster_fit_names)
            word_cloud_fig = grouped_wordcloud_fig.add_subplot(n_clusters // 2 + n_clusters % 2,2,i)
            word_cloud_fig.imshow(word_cloud.recolor(color_func=lambda *args, **kwargs: colors[i%len(colors)], random_state=42), interpolation='bilinear')
            word_cloud_fig.axis('off')
            extent = word_cloud_fig.get_window_extent().transformed(grouped_wordcloud_fig.dpi_scale_trans.inverted())
            grouped_wordcloud_fig.savefig(fp(f"{RUN_NAME}_{cluster_fit_name}.png"), bbox_inches=extent, dpi=1000)
            # word_cloud_fig.show() doesn't work as a subplot
        grouped_wordcloud_fig.savefig(fp(f"{RUN_NAME}_clusters.png"), dpi=1400)